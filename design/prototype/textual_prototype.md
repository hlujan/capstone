## Prototype Diagrams - Level 0
## ABET Information Management System

This document contains textual forms of prototype diagrams.
These diagrams are intended to show what functionality should be present on a given screen of the web application. Not every screen is shown, but an attempt was made to select at least one representative screen of a certain similar type of screen. For example, there will be pages for editing the different kinfs of course forms, but only one such form is shown.

It is critical to understand that these documents display a very basic skeleton of what content would appear and makes no attempt to be visually appealing. This is only to ensure that the program's required function are present in the system. 


**Tasks**


- User's assigned tasks appear here
- User could click on a task to be taken to the information, document
- By default, only shows tasks still in progress but use may choose to show completed as well
- A number of options for filtering results available



         @startsalt
         {+
         {* <b>tasks | browse | logoff }
         
         {+
         "course    " | "term    " | "type    " | "status   "
         }
         {+
         Assigned: {"Year  " | "Month  " | "Day   "}
         Due: {"Year  " | "Month  " | "Day   "}
         }
         
         {+
         [filter] | [clear filters]
         }
         ...
         {#
         course | term | type | assigned | due | status
         cosc 4342.001 | fall 2016 | eocm | 10-9-16 | 12-5-16 | create
         cosc 1436.003 | fall 2016 | eocm | 10-9-16 | 12-5-16 | create
         cosc 4342.001 | fall 2016 | pie | 10-9-16 | 12-5-16 | submitted
         cosc 1436.001 | fall 2016 | pie | 10-9-16 | 12-5-16 | submitted
         }
         
         {
         <&arrow-circle-left> | <&arrow-circle-top> | <&arrow-circle-right>
         }
         
         {+
         [] show approved
         }
         ...
         save as:
         {
         [csv] | [todo]
         }
         
         }
         @endsalt



**document**


- Every document in the system has such a page
- Contains variety of metadata
- Contains whatever options are appropriate such as Edit, Approve, etc depending on the state of the document and rights of the viewer
- Can display all attached links. A links pop-up could show every item that this document references as well as all items that reference this document. 
- Can display all file attachments associated with document
- Can display message log of comments made on the document



         @startsalt
         {
         {* tasks | <b>browse | logoff }
         
         {
         type: {"end of course memo"} | status: {"create"}
         }
         ...
         {
         degree: {"computer science"} | option: {"systems programming"}
         }
         {
         course: {"cosc 1435.001"} | term: {"fall 2016"}
         }
         ...
         {
         assigned: {"10-9-16"} | due: {"12-5-16"}
         }
         ===
         {
            [view] | [resolve] | [edit]
         }
         ===
         {
            links: <&caret-bottom> | attachments: <&caret-bottom> | comments: <&caret-bottom>
         }
         ...
         {
            assigned by: {"christine allman"} | assigned to: {"larry young"}
         }
         }
         @endsalt



**form: end of course memo**


- An example of a form document
- Read/write permissions assigned as appropriate



         @startsalt
         {
         {* <b>tasks | browse | logoff }
         
         {
         <b>form type: {"end of course memo"}
         }
         ===
         {
         semester: {"fall 2016"}
         }
         {
         instructor: {"larry young"}
         }
         ...
         {
         program: {"systems programming"} | course code: {"76457"} | course title: {"problem solving with computers i"}
         }
         ...
         {#
         . | <b>course outcomes | <b>outcome rating
         1 | experiment and interpret | good | {<&caret-bottom>}
         2 | formulate and solve problems | excellent | {<&caret-bottom>}

         }
         ...
         {
         <b>comments about course outcomes
         }
         {
         "                              "
         }
         ...
         {
         <b>list the aspects of the course that were most successful
         }
         {
         "                              "
         }
         ...
         {
         <b>list what did not work as well as intended
         }
         {
         "                              "
         }
         ...
         {
         <b>what changes should be made to the course?
         }
         {
         "                              "
         }
         ...
         {
         <b>what changes were recommended in previous reports?
         }
         {
         "                              "
         }
         ...
         {
         <b>what changes were implemented?
         }
         {
         "                              "
         }
         ...
         {
         <b>what were the results of the changes?
         }
         {
         "                              "
         }
         ...
         {
         <b>Additional Comments
         }
         {
         "                              "
         }
         ===
         {
         [Submit] | [Exit] | [Attach File] 
         }
         
         }
         @endsalt


**Master Matrix**


- May select which master matrix to view based on discipline and track
- Coordinator may select fields to assign ABET Course Outcomes to courses
- Can compute if minimum courses for an outcome are met upon saving


         @startsalt
         {
         {* <b>tasks | browse | logoff }
         {
         Master Matrix
         }
         ===
         {
         Discipline: {[computer science]} | Track: {[systems programming]} | Term {"Fall 2016"}
         }
         ===
         {#
         . | . | Course Name | Design with Constraints | Multidisciplinary Teams | Broad Education | Formulate and Solve Problems | .
         COSC | 1435 | Problem Solving with Computers I | X | . | X | . | 2
         COSC | 1436 | Problem Solving with Computers II | . | X | . | . | 1
         . | . | Total Required Courses | 3 | 2 | 4 | 2 | 11  
         }
         {
            [Update] | [Exit] 
         }
         ...
         save as:
         {
            [pdf] | [csv]
         }
         }
         @endsalt


**Browse**


- Similar to the user's Tasks page, but shows all documents that a particular user has permission to view regardless of instructor
- More tools for filtering that Tasks
- Selection options for the coordinator to perform batch operations on documents


         @startsalt
         {+
         {* <b>tasks | browse | logoff }
         {+
         "discipline    " | "track     " | "instructor   "
         }
         {+
         "course    " | "term    " | "type    " |  "status   "
         }
         {+
         Assigned: {"Year  " | "Month  " | "Day   "}
         Due: {"Year  " | "Month  " | "Day   "}
         }
         
         
         {+
         [filter] | [clear filters]
         }
         ...
         {#
         course | term | type | instructor | assigned | due | status | select
         cosc 4342.001 | fall 2016 | EOCM | Larry Young | 10-9-16 | 12-5-16 | create | [X]
         cosc 1436.003 | fall 2016 | EOCM | Scott King | 10-9-16 | 12-5-16 | create | [ ]
         cosc 4342.001 | fall 2016 | PIE | Larry Young | 10-9-16 | 12-5-16 | submitted | [ ]
         cosc 1436.001 | fall 2016 | PIE | Scott King  | 10-9-16 | 12-5-16 | submitted | [X]
         }
         
         {
         <&arrow-circle-left> | <&arrow-circle-top> | <&arrow-circle-right>
         }
         
         {#
         [] show approved | save selected to.. {^Bookmarks^}
         }
         ...
         save as:
         {
         [csv] | [todo]
         }
         
         }
         @endsalt


**Manage Users**


- Only for administrator account
- Can manually add a user 
- Can upload users from a CSV
- Can download current users to CSV to back them up
- Can perform batch operations on users
- Can manage what documents a particular user is allowed to be an Approver for
- Courses would be managed similarly



         @startsalt
         {
         {* <b>tasks | browse | manage | logoff }
         { [Users] | [Courses] }
         
         {#
         User Name | Type | Email | Select
         Katangur, Ajay | instructor | ajay.katangur@tamucc.edu | [X]
         King, Scott | instructor | scott.king@tamucc.edu | [ ]
         }
         ...
         {^List Approval Items^}
         ===
         {
         [add user]
         [import from file] | [export to file]
         }
         }
         @endsalt


**tracking: Action Item Log

         @startsalt
         {
         {* <b>tasks | browse | logoff }
         
         {
         <b>Tracking Sheet: {"Action Items"} |  {<&caret-bottom>}
         }
         ===
         {#
         Department | Year | # | Description of Action Item & Status | Lead Assignee | Other Assignees | Date Assigned | Source | Status | Completion Date
         ENG&T | 2016 | 021 | qwertyuiopasdfghjklzxcvbnm | Howard Lujan | Ryan Patton | 10-12-2016 | qwerty | open | N/A
         }
         ===
         {
         [CSV]
         }
         
         }
         @endsalt

