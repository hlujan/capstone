@startuml
autonumber
title Authentication

actor Instructor
boundary Login
boundary Library
control Authenticate
entity CAS
entity User_Table

Instructor -> Login : Request to login with CAS
activate Login
Login -> Authenticate : Forward request
activate Authenticate
Authenticate -> CAS : Query CAS server
activate CAS
CAS -> Authenticate : Send successful message
deactivate CAS
Authenticate -> Login : Send success message
deactivate Authenticate
Login -> Instructor : Display successful login message
Login -> Interface : Request Library
deactivate Login
activate Interface
Interface -> Library : Transfer to Library
deactivate Interface
Library -> Instructor : Display outstanding documents
deactivate Library

@enduml

@startuml
autonumber

title View and Assign\nABET Outcomes to Courses

actor  Coordinator
boundary Master_Matrix
control Manage_Master_Matrix
entity Master_Matrix_Tables

Coordinator -> Master_Matrix : Select Degree and Track
activate Master_Matrix
Master_Matrix -> Manage_Master_Matrix : Send request
activate Manage_Master_Matrix
Manage_Master_Matrix -> Master_Matrix_Tables : Select Master Matrix that corrosponds to selected (Degree, Track)
activate Master_Matrix_Tables 
Master_Matrix_Tables -> Manage_Master_Matrix : Send selected Master Matrix data
deactivate Master_Matrix_Tables
Manage_Master_Matrix -> Master_Matrix : Send data
deactivate Manage_Master_Matrix 
Master_Matrix -> Coordinator : Display data as table
deactivate Master_Matrix
Coordinator -> Master_Matrix : Click a field to assign outcome to course
activate Master_Matrix
Master_Matrix -> Manage_Master_Matrix : Send selected field
activate Manage_Master_Matrix
Manage_Master_Matrix -> Master_Matrix_Tables : Send update
activate Master_Matrix_Tables
Master_Matrix_Tables -> Manage_Master_Matrix : Send acknowledgement
deactivate Master_Matrix_Tables
Manage_Master_Matrix -> Master_Matrix_Tables : Request number of required courses for that outcome
activate Master_Matrix_Tables
Master_Matrix_Tables -> Manage_Master_Matrix : Send number of required courses
deactivate Master_Matrix_Tables
Manage_Master_Matrix -> Master_Matrix : Inform that required number not met
deactivate Manage_Master_Matrix
Master_Matrix -> Coordinator : Indicate that required number not met
deactivate Master_Matrix

@enduml


@startuml
autonumber

title Download Master Matrix

actor  Coordinator
boundary Master_Matrix
control Manage_Master_Matrix
entity Master_Matrix_Tables

Coordinator -> Master_Matrix : Select Degree and Track
activate Master_Matrix
Master_Matrix -> Manage_Master_Matrix : Send request
activate Manage_Master_Matrix
Manage_Master_Matrix -> Master_Matrix_Tables : Select Master Matrix that corrosponds to selected (Degree, Track)
activate Master_Matrix_Tables 
Master_Matrix_Tables -> Manage_Master_Matrix : Send selected Master Matrix data
deactivate Master_Matrix_Tables
Manage_Master_Matrix -> Master_Matrix : Send data
deactivate Manage_Master_Matrix 
Master_Matrix -> Coordinator : Display data as table
deactivate Master_Matrix
Coordinator -> Master_Matrix : Request to download as PDF
activate Master_Matrix 
Master_Matrix -> Manage_Master_Matrix : Send request
activate Manage_Master_Matrix 
Manage_Master_Matrix -> Manage_Master_Matrix : Convert view to PDF
Manage_Master_Matrix -> Master_Matrix : Send file
deactivate Manage_Master_Matrix
Master_Matrix -> Coordinator : Send file
deactivate Master_Matrix 

@enduml

@startuml
autonumber

title Search For and View\nDocument Metadata

actor Instructor
boundary Library
boundary Document
control Browse_Document 
control Manage_Document
entity Document_Tables

Instructor -> Library : Select to show approved documents
activate Library
Library -> Browse_Document : send request
activate Browse_Document 
Browse_Document -> Document_Tables : select all documents available to user
activate Document_Tables
Document_Tables -> Browse_Document : send document data
deactivate Document_Tables
Browse_Document -> Library : send document data
deactivate Browse_Document
Library -> Instructor : display document list as table
deactivate Library

Instructor -> Library : Send filter for Fall 2014 End of Course Memos
activate Library 
Library -> Browse_Document : send request
activate Browse_Document
Browse_Document -> Library : apply filter, send
deactivate Browse_Document
Library -> Instructor : display modified document list as table
deactivate Library

Instructor -> Library : Send document selection
activate Library
Library -> Interface : Request document
activate Interface 
Interface -> Document : Transfer to Document, send selected document 
deactivate Interface
deactivate Library
activate Document 
Document -> Manage_Document : Request document
activate Manage_Document 
Manage_Document -> Document_Tables : Send request
activate Document_Tables 
Document_Tables -> Manage_Document : Send document metadata
deactivate Document_Tables
Manage_Document -> Document : Send document metadata
deactivate Manage_Document 
Document -> Instructor : Display document metadata
deactivate Document

@enduml

@startuml
autonumber

title Fill Out Form

actor Instructor
boundary Library
boundary Document
boundary Form
control Browse_Document 
control Manage_Document
control Manage_Form
entity Document_Tables

note over Instructor:  After Instructor logs in, is directed to Library\nview with only outstanding tasks shown


Instructor -> Library : Send document selection
activate Library
Library -> Interface : Request document
activate Interface 
Interface -> Document : Transfer to Document, send selected document 
deactivate Interface
deactivate Library
activate Document 
Document -> Manage_Document : Request document
activate Manage_Document 
Manage_Document -> Document_Tables : Send request
activate Document_Tables 
Document_Tables -> Manage_Document : Send document metadata
deactivate Document_Tables
Manage_Document -> Document : Send document metadata
deactivate Manage_Document 
Document -> Instructor : Display document metadata
deactivate Document

Instructor -> Document : Select to view form
activate Document
Document -> Interface : Request Form
activate Interface
deactivate Document
Interface -> Form : Transfer to Form, send selected form
deactivate Interface
activate Form

Form -> Manage_Form : request form data
activate Manage_Form
Manage_Form -> Form : Send field data for document
deactivate Manage_Form 
Form -> Instructor : Display, populate form
deactivate Form

Instructor -> Form : Submit completed form
activate Form 
Form -> Manage_Form : Send data
activate Manage_Form 
Manage_Form -> Document_Tables : Update form row
activate Document_Tables 
Document_Tables -> Manage_Form : Send acknowledgement
deactivate Document_Tables
Manage_Form -> Form : Send acknowledgement
deactivate Manage_Form 
Form -> Interface : Request Document metadata
activate Interface
Interface -> Document : Transfer Document
deactivate Form
deactivate Form
activate Document
Document -> Instructor : Display Document metadata
deactivate Document


@enduml


