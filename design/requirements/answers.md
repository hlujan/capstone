## Requirments Gathering
## Questions for Client

This document contains answers to questions that were asked during meetings with the client. 
Of course, some answers are complex or incomplete and may not be fully represented in this document, 
but the material here should serve to document some results of requirements gathering.


### Overall Design

Perfect end product description?
- This will help us picture a long term goal and give a direction that will help resolve additional questions.

        Too "big" for this doc

Minimum end product description?
- In order to prioritize, what minimum system would be considered usable and helpful?

        Too "big" for this doc

Wish list priorities?

        Too "big" for this doc
        That said, the propossed plan is to first create the requirements.
        Then, present those requirements to client for sorting by priority
        

Organization/flow of page. Discuss/create a typical users use case.

        In brief:
        Admin login --> check documents status --> see that Dr. Thomas has not submitted action item
        Admin login --> go to admin panel --> remove retired professor
        Admin login --> go to matrices --> select mech. eng. --> click on a field to assign an outcome to a course
        Admin login --> notified that outcome "Knowlege", degree "mech. eng." is less than required # courses. 
        Prof:

What are the action items for?

        An action item is essentially an issue that requires an action. Corrosponding form is "Action Item Response Form"

What type of information are you looking for with the “Tracking Sheet” for action items? 
- Does this just track when action items are emailed to approvers and when those items are signed?

        Action items should be tracked: stages such as "need to submit", "waiting approval", "complete"
        Send out monthly reminders

Counted 10 different pages, more or less or just that info?

        Just that.

In action Items What exactly do you mean with “ability to hyperlink source documents for the action item”?

        If an action item corrosponds to a particular document, should be able to click on the action item or corrosponding button to navigate.
    
### ABET Specifications

Are the needs for the Different ABET accreditations roughly the same? 
- (Systems Programming, GIS, Mechanical Engineering, Electrical Engineering, and Mechanical Engineering Technology)

        Largely the same. More specific engineering fields inherit Engineering. Should build system in such a way that subclasses are trivial to add by the user.

And are other similar accredation systems roughly similar such that a generalized design would be appropriate?
- Programming broadly is nice as long as it does not hinder the capability of the target platform: ABET

        Do not need to use "Abet" name in hard-coded values. But the various forms (matrix, action items, etc) have rather specific ways that they interact with the users and DB. 
        Should be built for ABET. 

Where do the current ABET forms come from (Performance Indicator, End of Course Memo, Action Item Response Form)? 
- Are they Web Forms or are they PDF files that are being sent electronically?

        They were PDFs being sent by e-mail. In our system, will be web forms. 

The slides state that master matrices are used to assign specific courses to assess ABET Student Outcomes, do these courses change often, or is it fairly consistent at TAMUCC?

        The courses change little. The outcomes change little. The (course, outcome) tuple changes frequently. 
        This is part of why we believe that we can implement a DB from scratch, rather than connecting to the university.
        Since the data is instructors, courses, outcomes.. does not change often. Would be better to have the user input and remove these manually from an admin form,
        rather than build a hacky link to SAIL/BB. 

Does ABET require specific Wording for the Performance Indicator Evaluation Forms provided by instructors?
- And where can this, and other wording specifications, be found?

        All in the larger documentation we were given. 

Do the required forms for instructors change based off of the Mapping Matrix, and is a different mapping matrix needed for each major?
- Where can matrix fields be found?

        Matrix Fields will be input by user into DB via form interaction. 
        One matrix for each major. The matrix should be part of the highest "class" such that "mechanical eng" inherets "eng".

### Database and File Structure

Resources available. Database, server side products.

        At the moment, being done without DB. 
        We do not have any specific resources available. 
        That said, not that many data points since students are not part of it. Would be better to have our own DB in which the courses, outcomes, etc are manually added.
        Could have a separate script that populates DB, but should not make program reliant on connection with SAIL/BB/etc

Storage, if people can upload PDF’s and pictures.

        Yes.

Does ABET require specific file names, or does naming conventions just aid with the identification of files?

        Was neither asked nor answered. I do not think that she would know, so I assume not.
        Would likely by better to have filenames that aid automation

You stated that some fields would need to be enlarged to handle larger answers, do you have an estimate on what the largest answers would be? 
- (Character/word count)

        Answer pending.

Do you have any current system for archiving old documents, or is this just manually creating backups?
- Archive forever?
- It is mentioned that there should be a history for the end of the course memos, but where are these memos going to be accessed/stored?

        Yes, should archive forever. Should have ability to manually prune.
        Store all data in own DB. Uni can choose to back up along with their other systems

For your Wish List for “End Of Course Memos”, when you mean accessing previous reports for each report, would this be course based- such as previous reports for similar course, or previously submitted reports that where not currently approved? 
- Would an Archived Database for all passed forms be suitable for your needs?

        When a prof logs in, should see all Memos for courses that they teach. On a given memo, should be able to browse memo history. 

On the matrices how are we going to gather information from sail?
- It would be great if it could be explained thoroughly what information goes where, and where exactly do we gather the information for each of the fields. 
- IE where do we get the Abet Outcome, Performance Indicator, target information, and or all the information on the performance indicator form.
- Would be great if we could see these forms already filled so we can have an idea of what information they hold on each field

        We don't, as previously explained. 

### Communications

Are reports first sent to instructors to fill out, or is this something that they start on their own?

        Should automatically send out at specific time for all documents. 

It is mentioned that emails need to be sent to authors for the end of the course memos, are these emails for remainders going to be send automatically? And emails for comments will be sent manually by people?

        Automatically. Send auto-reminders periodically. (Should allow a reminder interval for each type of doc)

### Users and Priviledges 

Is there a hierarchy of permissions on the database? 
- On the end course memo and on the performance matrix it is mentioned that review status for the end of course memos will be changing status, only specific people can change this status?
- Other faculty members are going to be able to see other’s performance matrixes and end of the course memos?
- Also it is mentioned as an option to send comments to authors, again only specific people have access to this option?
- The project needs accounts that have special permissions to manipulate/edit/add the end of the course memos?

        Answer pending. 

Differences between groups not listed?

        ???

