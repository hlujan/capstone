## Requirments Gathering
## Questions for Client

This document contains questions that will be asked during meetings with the client. 
Answers to those questions will be added as they arrive.
Of course, some answers are complex or incomplete and may not be fully represented in this document, 
but the material here should serve to document some results of requirements gathering.


### Overall Design

Perfect end product description?
- This will help us picture a long term goal and give a direction that will help resolve additional questions.

Minimum end product description?
- In order to prioritize, what minimum system would be considered usable and helpful?

Wish list priorities?

Organization/flow of page. Discuss/create a typical users use case.

What are the action items for?

What type of information are you looking for with the “Tracking Sheet” for action items? 
- Does this just track when action items are emailed to approvers and when those items are signed?

Counted 10 different pages, more or less or just that info?

In action Items What exactly do you mean with “ability to hyperlink source documents for the action item”?

### ABET Specifications

Are the needs for the Different ABET accreditations roughly the same? 
- (Systems Programming, GIS, Mechanical Engineering, Electrical Engineering, and Mechanical Engineering Technology)

And are other similar accredation systems roughly similar such that a generalized design would be appropriate?
- Programming broadly is nice as long as it does not hinder the capability of the target platform: ABET

Where do the current ABET forms come from (Performance Indicator, End of Course Memo, Action Item Response Form)? 
- Are they Web Forms or are they PDF files that are being sent electronically?

The slides state that master matrices are used to assign specific courses to assess ABET Student Outcomes, do these courses change often, or is it fairly consistent at TAMUCC?

Does ABET require specific Wording for the Performance Indicator Evaluation Forms provided by instructors?
- And where can this, and other wording specifications, be found?

Do the required forms for instructors change based off of the Mapping Mattrix, and is a different mapping matrix needed for each major?
- Where can matrix fields be found?

### Database and File Structure

Resources available. Database, server side products.

Storage, if people can upload PDF’s and pictures.

Does ABET require specific file names, or does naming conventions just aid with the identification of files?

You stated that some fields would need to be enlarged to handle larger answers, do you have an estimate on what the largest answers would be? 
- (Character/word count)

Do you have any current system for archiving old documents, or is this just manually creating backups?
- Archive forever?
- It is mentioned that there should be a history for the end of the course memos, but where are these memos going to be accessed/stored?

For your Wish List for “End Of Course Memos”, when you mean accessing previous reports for each report, would this be course based- such as previous reports for similar course, or previously submitted reports that where not currently approved? 
- Would an Archived Database for all passed forms be suitable for your needs?

On the matrices how are we going to gather information from sail?
- It would be great if it could be explained thoroughly what information goes where, and where exactly do we gather the information for each of the fields. 
- IE where do we get the Abet Outcome, Performance Indicator, target information, and or all the information on the performance indicator form.
- Would be great if we could see these forms already filled so we can have an idea of what information they hold on each field

### Communications

Are reports first sent to instructors to fill out, or is this something that they start on their own?

It is mentioned that emails need to be sent to authors for the end of the course memos, are these emails for remainders going to be send automatically? And emails for comments will be sent manually by people?

### Users and Priviledges 

Is there a hierarchy of permissions on the database? 
- On the end course memo and on the performance matrix it is mentioned that review status for the end of course memos will be changing status, only specific people can change this status?
- Other faculty members are going to be able to see other’s performance matrixes and end of the course memos?
- Also it is mentioned as an option to send comments to authors, again only specific people have access to this option?
- The project needs accounts that have special permissions to manipulate/edit/add the end of the course memos?

Differences between groups not listed?


