# ABET Form Management System



## 1. Project Overview and Objectives


### Project Overview


The project is to design and implement a web application that will manage the various ABET-related forms between the instructors and the ABET coordinator. ABET is an accreditation agency that has accredited a number of degree plans in the engineering and computer science disciplines. In order to maintain accreditation, instructors are responsible for evaluating how effectively the courses that they teach demonstrate that students are meeting the ABET student outcomes. This program will facilitate form tracking and archiving as well as maintaining consistency through auto-population of many fields.


### Objectives


This program was proposed because of issues related to the way that ABET documents are currently handled. Forms are e-mailed back and forth from the instructors and the ABET coordinator. Reports are frequently forgotten, given inconsistent file names, and are not filled out with consistent wording. The result is that the ABET coordinator has to spent a significant amount of time tracking down files, determining who has submitted their forms, rewriting fields to use proper ABET wording and other menial tasks that could be alleviated through a more automated and trackable system.



## 2. Requirements


### Plain-English Description


### Overview of relevant ABET forms


As this system is focused on managing a select set of ABET forms, these forms will be referenced throughout this text. The following presents brief descriptions of these forms including the name and acronym by which they may be later referred. 


*Master Matrices (MM):*

This is a table such that each row is a particular course and each column is an ABET Student Outcome. The ABET coordinator assigns ABET Student Outcomes to particular courses by marking the field that corresponds to that course and Student Outcome. There is a master matrix for each degree plan and this matrix is what ultimately drives the rest of the ABET form system as it directs how courses are expected to meet accreditation requirements. 


*End of Course Memo (ECM):*

This form is used to gauge how well a particular course was able contribute to a specific set of assigned ABET Student Outcomes. The instructor selects an outcome rating such as "Excellent" or "Needs Improvement" for each of the assigned ABET Student Outcomes. The instructor is then prompted to answer a number of questions about the course such as to list successful aspects of the course and to suggest changes that should be made. 


*Performance Indicator Evaluation (PIE):*

This form is used to evaluate particular items in a course. Performance Indicators are specific aspects of a class that demonstrate student's meeting the target ABET expectation.


*Action Item Response Form (AIR):*

This form is used to address issues that are discovered through the End of Course Memos and Performance Indicator Evaluations or by other means. Action items are issued and then tracked in order to see how the particular issue is being dealt with. 


### Functional Requirements 

The overall purpose of the system is to lessen tedious paper management for the ABET coordinator and instructors as well track form status and maintain a document history. 

The ABET coordinator must be able to do the following:
- Assign ABET Student Outcomes to courses using the Master Matrices 
- Review submitted forms and either accept or reject the submission
- Provide comments on forms that will be sent to the form submitter
- Track the status of forms
- Generate action items that will be sent to instructors and associated with a particular course
- Track completed and in-progress action items
- Alert instructors as needed, either manually or by setting auto-reminders
- Ability to annotate documents with notes and files as well as edit those annotations

Instructors must be able to do the following:
- Fill out and submit forms
- Receive feedback for forms
- Track own submission status and see what forms remain to be filled
- Receive form reminders

Instructors and the ABET coordinator must be able to do the following:
- Browse a history of forms
- View the master matrices to see what ABET Student Outcomes are assigned to what courses
- View and respond to action items
- Produce reports by saving completed forms as PDFs


### Non-functional Requirements

The server must be able to host a web server and run a website that employs server-side processing and database interaction. Further, the server must be able to host a MySQL database that accepts connections from the website. The database and web server may be hosted on separate machines, but must be able to communicate such that the web server may authenticate and utilize the database. The web server must allow connections from clients on and off campus. 

The system should be able to import a file containing instructor, course and ABET information directly so that that system does not depend on being able to communicate with and pull data from SAIL or Blackboard or other existing university services. This is to ensure that the ABET Information Management System is not dependent on other systems outside of its own database. Any usage of such outside resources much be optional, such as a possible option to use CAS for authentication. 

### Stuctured Requirements

*User and System Management*

1. The User should be allowed to log into the system.

    1. The system should authenticate the Users credentials and allow access.

2. The Coordinator should be allowed to log into the system with administrative privileges, manage users, and manipulate system data.

    2. The Coordinator should be allowed to manage users.

        1. The Coordinator should be allowed to add a new user to the system.

        2. The Coordinator should be allowed to remove a current user from the system.

    2. The Coordinator should be allowed to manipulate system data.

        1. The Coordinator should be allowed to import system data from a file.

        2. The Coordinator should be allowed to export system data to a file.

*Master Matrices (MM)*

1. The User should be allowed to view and export the current MM.

    1. The User should be able to view all courses and their corresponding active outcomes.

    2. The User should be allowed to view the current status of active course outcomes.

    3. The User should be able to export the current matrix to PDF.

2. The Coordinator should be allowed to do everything the user can do. The Coordinator may also create/edit and activate/deactivate information in the MM.

    1. The Coordinator should be allowed to create/edit courses in the MM.

    2. The Coordinator should be allowed to activate/deactivate courses in the MM.

    3. The Coordinator should be allowed to create/edit course outcomes in the MM.

    4. The Coordinator should be allowed to activate/deactivate course outcomes in the MM.

*End of Course Memo (ECM)*

1. The User should be allowed to view, edit, submit and export ECM’s.

    1. The User should be able to view their active ECM’s, and edit certain fields.

        1. The User should be allowed to view the status of an ECM.

        2. The User should be allowed to receive reminders regarding an ECM,

        3. The User should be allowed to edit the List Course Outcomes field.

        4. The User should be allowed to edit the Outcome Rating field.

        5. The User should be allowed to add comments to the course evaluation question comment fields.

        5. The User should be allowed to add comments to the Additional Comments field.

    2. The User should be allowed to view past ECM’s.

        1. The User should be allowed to view past ECM’s by course.

    3. The User should be allowed to submit a complete ECM.

    4. The User should be allowed to export and ECM to PDF.

2. The Coordinator should be allowed to do everything the user can do except submit forms and receive reminders. The Coordinator may also issue ECM’s, reminders and edit ECM’s and their content.

    1. The Coordinator should be allowed to issue an ECM.

        1. The Coordinator should be allowed to create a new ECM.

        2 The Coordinator should be allowed to edit the Instructor field.

        3. The Coordinator should be allowed to edit the Semester field.

        4. The Coordinator should be allowed to edit the Program/Course Code/Course Title field.

        5. The Coordinator should be allowed to edit the Outcome Rating Rubric field.

        5. The Coordinator should be allowed to edit the course evaluation questions.

    2. The Coordinator should be able to edit an ECM.

        1. The Coordinator should be allowed to perform all the edits under section a. on an active ECM.

        2. The Coordinator should be allowed to perform all the edits under section a. on an inactive or archived ECM.

        3. The Coordinator should be allowed to remove an active, inactive, or archived ECM

*Performance Indicator Evaluation (PIE)*

1. The User should be allowed to edit, attach files to, submit and export an active PIE as well as receive messages about an active PIE.

    1. The User should be allowed to edit specific fields of an active PIE.

        1. The User should be able to edit the Performance Indicator field.

        2. The User should be able to edit the Description of Performance Indicator field.

        3. The User should be allowed to input the evaluation percentages.

        4. The User should be allowed to input the Target Score.

    2. The User should be allowed to attach files to an active PIE.

    3. The User should be allowed to submit a complete PIE for review.

    4. The User should be allowed to export a PIE to PDF.

    5. The User should be allowed to receive messages about an active PIE.

    6. The User should be allowed to check the status of an active PIE.

2. The Coordinator should be allowed to do everything the user can do except submit a PIE. The Coordinator may also select PIE recipients, review a submitted PIE, accepts or reject a submitted PIE and send comments about a reviewed PIE.

    1. The Coordinator should be allowed to select the recipient of a PIE.

    2. The Coordinator should be allowed to review a submitted PIE.

        1. The Coordinator should be allowed to accept or reject a submitted PIE.

        2. The Coordinator should be allowed to send comments to the recipient of an evaluated PIE.

*Action Item Response Form (AIR)*

1. The User should be allowed to view and select an AIR.

    1. The User should be allowed to view an active AIR.

        1. The User should be allowed to select that AIR and follow its instruction.

2. The Coordinator should be able to view, edit, check status, and issue all AIR’s.

    1. The Coordinator should be allowed to issue an AIR.

        1. The Coordinator should be allowed to edit all appropriate fields of an AIR.

        2. The Coordinator should be allowed to select the AIR recipient.

    2. The Coordinator should be allowed to view an AIR.

        1. The Coordinator should be allowed to check the current status of an AIR.

            1. The Coordinator should be allowed to select the action item associated to the AIR and review its contents.

        2. The Coordinator should be allowed to edit an active AIR.

            1. The Coordinator should be allowed to set an AIR to complete. 
