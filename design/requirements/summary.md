## ABET - System Overview

This document describes the major entities related to current document system.

Throughout, opportuntities for automation or other implementation details noted. 

ABET is an accredetation agency for STEM degrees.

abet.org

## Problem:

ABET involves a number of documents. These documents are forms filled in by instructors and approved by the ABET coordinator. 

Currently, the system is a hassle because forms are being sent both ways as PDFs over email. There is a great deal of manual entry for
the instructors and the ABET coordinator. The manual entry promotes the issue that instructors are not using official ABET wording, 
which makes organization difficult and forces the ABET coordinator to redo much of it. 

There is also no comfortable way to browse a history of related forms. 

The goal of this program is to have a web application that handles the forms, autopopulates fields when appropriate, track form progress and maintain history.

Related documents should be hyperlinked and files should be able to be attached to documents. 


## Degree Plans

There are, as far as we can see, three levels of ABET degree plan organization.

There is a general "Engineering Student Outcomes" common to all eng. degree plans.

Then there are specific Eng. plans that extend the general. 

EX: mech. eng. inherits course outcomes from eng. and adds own outcomes

There are also very high level "student outcomes" common to all plans.

## End of Course Memo

This is, perhaps, the main form in the system.

Instructors will fill one out for each course that they teach that is relevant to ABET.

This form is used to gauge how effective each course outcome associated with the course was.

One area on the form needs explaining: "Program, Course Code, Course Title". 

That section has 3 rows, but it is for one course? This is because a single "class" may serve
as a different "course" for different students. 

EX: A mech. eng. and mech. tech. eng. may both take Statics from same prof as same time, but
it count as two separate courses in their respective degree plans.

Oppurtunites for auto-population:
- Outcome Ratings: there are 5 of them. Should select from drop-down.
- Course Outcomes: Should already be defined by the master matrix?
- Instructer and Semester: drop-down

Should browse past memos for this course on the page. 

Should allow to upload a file.


## Master Matrices

These matrix forms define the entire system by assigning course outcomes to courses for each degree plan.

If (ENG-3315, Design with constraints) == true, then the End of Course Memo for ENG-3315 should list "Design with constraints"

Should ensure that "Total required courses" for each outcome is met. (Perhaps number in red if not?)


## Performance Indicator Evaluation Form

Performance Indicators are specific aspects of a class that demonstrate student's meeting the target ABET expectation.

There is a target score for the indicator. EX: Getting an 'A' on a computer networks team project would indicate "Communicate Effectively"

There is a target % of students: The percentage of students that have to meet that target score for the Performance Indicator to meet target.

There is a results % of students: How many actually did.

This is a "Target met/not met" boolean field: TRUE of students_% >= target_%

## Tracking Sheet (End of Course Memo and Performance Indicator Evaluations)

Shows the pass or fail of course evaluations and ABET course outcomes for each (instructor, course) pair. 

Are the ABET metrics auto-populated by whether or not the "Target met" is TRUE for the Performance Indicator Evaluation Form?

Where does "Course Evaluation" coume from?

## Tracking Reports

Need to see each instructor and the status of their reports. 

Status include "Sent", "Submitted", "Accepted", etc.

Send automatic reminders to instructors who have no submitted.

Should allow setting this reminder interval. 

## Action Item Response Form

If there is an issue, such as a Performance Indicator not being met, need to intiate an "action item" to correct it.

Opportunities for auto-population:
- Date Assigned: Upon publishing
- ABET Student Outcomes Associated.. : Drop-down


