## Use Case Diagrams 
## ABET Information Management System

This document contains textual forms of use case diagrams. 
The contents of this document may be converted to graphical diagrams using plantuml.

### Actors:
- Coordinator: ABET Coordinator
- Instructor: Faculty who fill out forms
- Database: Back-end data storage


### Level 0 

      @startuml
      left to right direction
      skinparam packageStyle rect
      actor Coordinator
      actor Instructor
      actor Evaluator
      actor Database
      rectangle ABET_Information_Management_System {
        
        Coordinator -- (Manage Evaluation Forms)
        Instructor -- (Manage Evaluation Forms)
        (Manage Evaluation Forms) -- Database
        
        Coordinator -- (Browse Documents)
        Instructor -- (Browse Documents)
        (Browse Documents) -- Evaluator
        (Browse Documents) -- Database
      
        Coordinator -- (Manage Master Matrices)
        (Manage Master Matrices) -- Evaluator
        (Manage Master Matrices) -- Database
      
        Coordinator -- (Manage Instructors and Courses)
        (Manage Instructors and Courses) -- Database
      
        Coordinator -- (Track Form Status)
        Instructor -- (Track Form Status)
        (Track Form Status) -- Database
      
        Coordinator -- (Manage Action Items)
        Instructor -- (Manage Action Items)
        (Manage Action Items) -- Database
      
        Coordinator -- (Log On to System)
        Instructor -- (Log On to System)
        (Log On to System) -- Evaluator
        (Log On to System) -- Database
      
      }
      @enduml


### Level 1 : Manage Master Matrices

      @startuml
      left to right direction
      skinparam packageStyle rect
      actor Coordinator
      actor Instructor
      actor Database
			actor Evaluator
      rectangle Manage_Master_Matrices {
         
         Coordinator -- (Assign Student Outcomes to Courses)
         (Assign Student Outcomes to Courses) -- Database
         
         Coordinator -- (View Master Matrices)
         Instructor -- (View Master Matrices)
         (View Master Matrices) -- Evaluator
         (View Master Matrices) -- Database
      
         Coordinator -- (Add Courses to Master Matrices)
         (Add Courses to Master Matrices) -- Database
      
         Coordinator -- (Save as PDF)
         Instructor -- (Save as PDF)
         (Save as PDF) -- Evaluator
         (Save as PDF) -- Database
      
      }
      @enduml

**Textual Description**


*Summary:* Create and Manage the Master Matrix used for evaluating course outcomes. Matrices are read-only for Instructor(s) and Evaluator.


*Actors:*
- Coordinator
- Instructor
- Database


*Preconditions:*
- An active connection to the Database
- Successful Log_on_to_System use case
- Existing Instructor and Course relationships in Database


*Flow of Events:*


        Main Flow - Coordinator
                Successful System Login
                View Master Matrix
                Assign Courses to specified Master Matrices - per term
                Assign Student outcomes to specified courses
                Student Outcomes to Course connections
                Course to Master Matrix connections
                (optional) Save Master Matrx as PDF

        Main Flow - Instructor, Evaluator
                Successful System Login
                View Master Matrix
                (optional) Save Master Matrix as PDF


*Post Conditions:*
- An active connection to the Database
- Successful Log_on_to_System use case
- Existing Instructor and Course relationships in Database


### Level 1 : Manage Evaluation Forms

      @startuml
      left to right direction
      skinparam packageStyle rect
      actor Coordinator
      actor Instructor
      actor Database
      rectangle Manage_Evaluation_Forms {
         
      Coordinator -- (Issue Form)
      (Issue Form) -- Database
      
      Instructor -- (Complete and Submit Form)
      (Complete and Submit Form) -- Database
      
      Coordinator -- (Edit Existing Form)
      (Edit Existing Form) -- Database
      
      Coordinator -- (Add/View Comments)
      Instructor -- (Add/View Comments)
      (Add/View Comments) -- Database
      
      Coordinator -- (Review Form)
      (Review Form) -- Database
      
      Coordinator -- (Save as PDF)
      Instructor -- (Save as PDF)
      (Save as PDF) -- Database
      
      }
      @enduml


**Textual Description**


*Summary:* Create, Issue, and Manage the Forms used for evaluating course outcomes. Forms are to be filed by Instructor.


*Actors:*
- Coordinator
- Instructor
- Database


*Preconditions:*
- An active connection to the Database
- Successful Log_on_to_System use case


*Flow of Events:*


        Main Flow - Coordinator
                Successful System Login
                - Pre Form Issue
                        Select form type to assign
                        Select Instructor(s) to assign
                        Select tasks for Instructor that is to receive form
                - Post Form Issue
                        View/Edit existing user form
                        (optional) Save form as PDF
                - Post Form Submission
                        View submitted forms
                        Select form to review
                        Review form based on assigned Matrix/Course outcomes
                        Accept or Reject Form submission
                        (optional) Attach comments to form submission to be sent to user
                        (optional) Save form as PDF

        Main Flow - Instructor
                Successful System Login
                - Post Form Issue
                        View assigned forms
                        Select an assigned form from list
                        Input data into form fields as required/assigned
                        (optional) Attach files to form
                        (optional) Save form as PDF
                        Submit complete form to Coordinator
                - Post Form Submission
                        Receive Status update of submitted form
                        (optional) Receive comments from coordinator
                        (optional) Save form as PDF
                        If rejected, edit and resubmit form


*Post Conditions:*
- An active connection to the Database
- Successful Log_on_to_System use case


### Level 1 : Browse Documents

      @startuml
      left to right direction
      skinparam packageStyle rect
      actor Coordinator
      actor Instructor
      actor Evaluator
      actor Database
      rectangle Browse_Documents {
         
      Coordinator -- (Search for Documents)
      Instructor -- (Search for Docments)
      (Search for Documents) -- Evaluator
      (Search for Documents) -- Database
      
      Coordinator -- (View Document Metadata)
      Instructor -- (View Document Metadata)
      (View Document Metadata) -- Evaluator
      (View Document Metadata) -- Database
      
      Coordinator -- (List All Documents)
      Instructor -- (List All Documents)
      (List All Documents) -- Evaluator
      (List All Documents) -- Database      
      
      Coordinator -- (Save as PDF)
      Instructor -- (Save as PDF)
      (Save as PDF) -- Database
      
      }
      @enduml


**Textual Description**


*Summary:* Instructor(s), Coordinator and Evaluator must be able to list all documents in the system such as End of Course Memos, Performance Indicator Evaluations and Action Items. All must be able to search using filters. All must be able to view and save documents that they have permission to.


*Actors:*
- Coordinator
- Instructor
- Evaluator
- Database


*Preconditions:*
- An active connection to the Database
- Successful Log_on_to_System use case


*Flow of Events:*


        Main Flow - Coordinator, Instructor, Evaluator
                Successful System Login
                - View a Document
                        Scroll through list of documents
                        (optional) Apply search filters
                        (optional) Select sorting parameters 
                        (optional) Clear search filters
                        Select document to view
                        See the document contents and associated metadata
                        (optional) Save document as PDF
                        Exit document



*Post Conditions:*
- An active connection to the Database
- Successful Log_on_to_System use case


### Level 1 : Track Form Status

      @startuml
      left to right direction
      skinparam packageStyle rect
      actor Coordinator
      actor Instructor
      actor Database
      rectangle Track_Form_Status {
         
         Coordinator -- (View Submissions Status)
         (View Submissions Status) -- Database
      
         Instructor -- (View Tasks)
         (View Tasks) -- Database
      
         Coordinator -- (Send/Receive Reminders)
         Instructor -- (Send/Receive Reminders)
         (Send/Receive Reminders) -- Database
      
      }
      @enduml

**Textual Description**

*Summary:* View list of forms submitted or awaiting submissions. Send reminder to users still awaiting submission.

*Actors:*
- Coordinator
- Instructor
- Database

*Preconditions:*
- An active connection to the Database
- Successful Log_on_to_System use case
- Assigned forms in Database

*Flow of Events:*


        Main Flow - Coordinator
                Successful System Login
                View Submission Status for all Current forms
                Select forms to send reminders to assigned users

        Main Flow - Instructor
                Successful System Login
                View currently assigned tasks/forms
                Receive email reminders for outstanding forms


*Post Conditions:*
- An active connection to the Database
- Successful Log_on_to_System use case
- Assigned forms in Database


### Level 1 : Manage Action Items

      @startuml
      left to right direction
      skinparam packageStyle rect
      actor Coordinator
      actor Instructor
      actor Database
      rectangle Manage_Action_Items {
         
         Coordinator -- (Issue Action Items)
         (Issue Action Items) -- Database
      
         Coordinator -- (Edit Action Items)
         (Edit Action Items) -- Database
      
         Coordinator -- (Approve Action Items)
         Instructor -- (Approve Action Items)
         (Approve Action Items) -- Database
         
         Instructor -- (Respond to Action Items)
         (Respond to Action Items) -- Database
      
      }
      @enduml


**Textual Description**


*Summary:* Create and edit action item to be assigned to Instructor forms.


*Actors:*
- Coordinator
- Instructor
- Database


*Preconditions:*
- An active connection to the Database
- Successful Log_on_to_System use case
- Assigned forms in Database


*Flow of Events:*


        Main Flow - Coordinator
                Successful System Login
                View list of current forms
                Select form to assign action item to
                Create action item(s) for unique form/course
                - Post Action Item Implementation
                        Mark action item as complete

        Main Flow - Instructor
                Successful System Login
                View list of currently assigned items to forms/course
                Select action item and view details of item
                Implement action items assigned course/form


*Post Conditions:*
- An active connection to the Database
- Successful Log_on_to_System use case
- Assigned forms in Database


### Level 1 : Manage System Instructors and Courses

      @startuml
      left to right direction
      skinparam packageStyle rect
      actor Coordinator
      actor Instructor
      actor Database
      rectangle Manage_System_Instructors_and_Courses {
         
         Coordinator -- (Add/Delete/Edit Instructors)
         (Add/Delete/Edit Instructors) -- Database
         
         Coordinator -- (Allow/Revoke Approver Privilege)
         Instructor -- (Allow/Revoke Approver Privilege)
         (Allow/Revoke Approver Privilege) -- Database
      
         Coordinator -- (Add/Delete/Edit Courses)
         (Add/Delete/Edit Courses) -- Database
      
         Coordinator -- (Import System Data From File)
         (Import System Data From File) -- Database
         
         Coordinator -- (Export System Data From File)
         (Export System Data From File) -- Database
      
      }
      @enduml


**Textual Description**


*Summary:* Allow Coordinator to manage Instructor(s) and Courses in the Database. Allow for file import/export. Allow Coordinator to give User(s) ability to approve specific Course(s).


*Actors:*
- Coordinator
- Database


*Preconditions:*
- An active connection to the Database
- Successful Log_on_to_System use case


*Flow of Events:*


        Mail Flow - Coordinator
        Successful System Login
        - Edit Instructors
                View list of current Instructors in Database
                Edit/Delete Record
                Create Unique new record
                (optional) Import File for New Instructors into the system
                (optional) Export Existing list of Instructors to file
        - Edit Courses
                View list of current Courses in Database
                Edit/Delete Record
                Create Unique new record
                (optional) Import File for New Courses into the system
                (optional) Export Existing list of Courses to file
        - Allow/Revoke Approver Privilege
                View list of current Instructors in Database
                View Courses that each Instructor is able to approve
                Add/Remove Courses that each Instructor is able to approve


*Post Conditions:*
- An active connection to the Database
- Successful Log_on_to_System use case


### Level 1: Log On to System

      @startuml
      left to right direction
      skinparam packageStyle rect
      actor Coordinator
      actor Instructor
      actor Evaluator
      actor Database
      rectangle Log_On_to_System {
         
         Coordinator -- (Attempt Authentication)
         Instructor -- (Attempt Authentication)
         (Attempt Authentication) -- Evaluator
      
         (Verify: Allow or Deny Access) -- Database
      
      }
      @enduml


**Textual Description**


*Summary:* Authentication process for accessing system by user(s), Evaluator and coordinator.


*Actors:*
- Coordinator
- Instructor
- Evaluator
- Database


*Preconditions:*
- An active connection to the Database


*Flow of Events:*


        Main Flow - 
                Access login webpage
                Provide username and password
                Verify username and password
                Proceed to ABET Information Management System

        Alternate Flow - Failed Instructorname/Password
                Access login webpage
                Provide username and password
                Failed Instructorname/Password
                Database notify user of wrong login credentials
                Return to login webpage


*Post Conditions:*
- An Active connection to the Database


### Level 2: Manage Evaluation Forms : Issue Form

      @startuml
      left to right direction
      skinparam packageStyle rect
      actor Coordinator
      actor Database
      rectangle Manage_Evaluation_Forms_:_Issue_Form {
         
         Coordinator -- (Select Form Type)
         (Select Form Type) -- Database
      
         Coordinator -- (Select Instructor Recipient)
         (Select Instructor Recipient) -- Database
      
         Coordinator -- (Set New Task For Instructor Recipient)
         (Set New Task For Instructor Recipient) -- Database
      
      }
      @enduml


### Level 2: Manage Evaluation Forms : Complete and Submit Form

      @startuml
      left to right direction
      skinparam packageStyle rect
      actor Instructor
      actor Database
      rectangle Manage_Evaluation_Forms_:_Complete_and_Submit_Form {
         
         Instructor -- (Browse Available Tasks)
         (Browse Available Tasks) -- Database
      
         Instructor -- (Select Form From Available Tasks)
         (Select Form From Available Tasks) -- Database
      
         (Auto-Populate Fields) -- Database
      
         Instructor -- (Input Data Into Fields)
         (Input Data Into Fields) -- Database
      
         Instructor -- (Attach Files)
         (Attach Files) -- Database
      
         Instructor -- (Submit Form)
         (Submit Form) -- Database
      
         (Update Task Status to "Submitted") -- Database
      
      }
      @enduml


### Level 2: Manage Evaluation Forms : Review Form 

      @startuml
      left to right direction
      skinparam packageStyle rect
      actor Coordinator
      actor Database
      rectangle Manage_Evaluation_Forms_:_Review_Form {
         
         Coordinator -- (Select Unreviewed Form Submission)
         (Select Unreviewed Form Submission) -- Database
      
         Coordinator -- (Accept or Reject Form Submission)
         (Accept or Reject Form Submission) -- Database
      
         Coordinator -- (Send Comments to Form Author)
         (Send Comments to Form Author) -- Database
      
         (Update Task Status to "Accepted" or "Rejected") -- Database
      
      }
      @enduml


